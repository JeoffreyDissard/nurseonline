<?php

namespace NurseBundle\Entity;

use \FOS\UserBundle\Model\User as FosUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends FosUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $prenom;

    /**
     * @var string
     */
    private $photo;

    /**
     * @var string
     */
    private $statusPro;

    /**
     * @var string
     */
    private $diplome;

    /**
     * @var string
     */
    private $employeur;

    /**
     * @var string
     */
    private $service;

    /**
     * @var string
     */
    private $competences;

    /**
     * @var string
     */
    private $qualites;

    /**
     * @var string
     */
    private $amelioration;

    /**
     * @var string
     */
    private $hobbit;

    /**
     * @var string
     */
    private $formation;

    /**
     * @var string
     */
    private $experience;

    /**
     * @var string
     */
    private $autreActivite;

    /**
     * @var string
     */
    private $objectif;


    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set statusPro
     *
     * @param string $statusPro
     *
     * @return User
     */
    public function setStatusPro($statusPro)
    {
        $this->statusPro = $statusPro;

        return $this;
    }

    /**
     * Get statusPro
     *
     * @return string
     */
    public function getStatusPro()
    {
        return $this->statusPro;
    }

    /**
     * Set diplome
     *
     * @param string $diplome
     *
     * @return User
     */
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return string
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Set employeur
     *
     * @param string $employeur
     *
     * @return User
     */
    public function setEmployeur($employeur)
    {
        $this->employeur = $employeur;

        return $this;
    }

    /**
     * Get employeur
     *
     * @return string
     */
    public function getEmployeur()
    {
        return $this->employeur;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return User
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set competences
     *
     * @param string $competences
     *
     * @return User
     */
    public function setCompetences($competences)
    {
        $this->competences = $competences;

        return $this;
    }

    /**
     * Get competences
     *
     * @return string
     */
    public function getCompetences()
    {
        return $this->competences;
    }

    /**
     * Set qualites
     *
     * @param string $qualites
     *
     * @return User
     */
    public function setQualites($qualites)
    {
        $this->qualites = $qualites;

        return $this;
    }

    /**
     * Get qualites
     *
     * @return string
     */
    public function getQualites()
    {
        return $this->qualites;
    }

    /**
     * Set amelioration
     *
     * @param string $amelioration
     *
     * @return User
     */
    public function setAmelioration($amelioration)
    {
        $this->amelioration = $amelioration;

        return $this;
    }

    /**
     * Get amelioration
     *
     * @return string
     */
    public function getAmelioration()
    {
        return $this->amelioration;
    }

    /**
     * Set hobbit
     *
     * @param string $hobbit
     *
     * @return User
     */
    public function setHobbit($hobbit)
    {
        $this->hobbit = $hobbit;

        return $this;
    }

    /**
     * Get hobbit
     *
     * @return string
     */
    public function getHobbit()
    {
        return $this->hobbit;
    }

    /**
     * Set formation
     *
     * @param string $formation
     *
     * @return User
     */
    public function setFormation($formation)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Get formation
     *
     * @return string
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Set experience
     *
     * @param string $experience
     *
     * @return User
     */
    public function setExperience($experience)
    {
        $this->experience = $experience;

        return $this;
    }

    /**
     * Get experience
     *
     * @return string
     */
    public function getExperience()
    {
        return $this->experience;
    }

    /**
     * Set autreActivite
     *
     * @param string $autreActivite
     *
     * @return User
     */
    public function setAutreActivite($autreActivite)
    {
        $this->autreActivite = $autreActivite;

        return $this;
    }

    /**
     * Get autreActivite
     *
     * @return string
     */
    public function getAutreActivite()
    {
        return $this->autreActivite;
    }

    /**
     * Set objectif
     *
     * @param string $objectif
     *
     * @return User
     */
    public function setObjectif($objectif)
    {
        $this->objectif = $objectif;

        return $this;
    }

    /**
     * Get objectif
     *
     * @return string
     */
    public function getObjectif()
    {
        return $this->objectif;
    }
}

