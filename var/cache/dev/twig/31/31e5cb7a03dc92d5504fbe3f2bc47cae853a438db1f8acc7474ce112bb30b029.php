<?php

/* @Nurse/Default/nav.html.twig */
class __TwigTemplate_a1a5accbcfa225d74f3c483a0cd59c7863799d9c59f2dc3d8b2d550ff137e9b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'nav' => array($this, 'block_nav'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Nurse/Default/nav.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Nurse/Default/nav.html.twig"));

        // line 1
        $this->displayBlock('nav', $context, $blocks);
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function block_nav($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        // line 2
        echo "    <ul id=\"dropdown1\" class=\"dropdown-content\">
        ";
        // line 3
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
        } else {
            echo "<li><a href=\"/login\">Se connecter</a></li>";
        }
        // line 4
        echo "        ";
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
        } else {
            echo "<li><a href=\"/register\">S'enregister</a></li>";
        }
        // line 5
        echo "        ";
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            echo "<li><a href=\"/logout\">Se déconnecter</a></li>";
        } else {
        }
        // line 6
        echo "    </ul>

    <nav>
        <div class=\"nav-wrapper cyan darken-3\">
            <a href=\"/\" class=\"brand-logo\">Journal</a>
            <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">
                <li><a href=\"/\">Accueil</a></li>
                ";
        // line 13
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            // line 14
            echo "                    ";
            if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_AUTEUR"), "method")) {
                // line 15
                echo "                        <li><a href=\"/user/ajouter\">Ajouter</a></li>
                    ";
            }
            // line 17
            echo "                    ";
            if ($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "hasRole", array(0 => "ROLE_ADMIN"), "method")) {
                // line 18
                echo "                        <li><a href=\"/auteur/ajouter\">Ajouter</a></li>
                        <li><a href=\"/admin/admin\">Admin</a></li>
                    ";
            }
            // line 21
            echo "                ";
        }
        // line 22
        echo "                <li><a class=\"dropdown-trigger\" href=\"#!\" data-target=\"dropdown1\">";
        if ($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array())) {
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
        } else {
            echo "Connexion";
        }
        echo "<i class=\"material-icons white-text right\">arrow_drop_down</i></a></li>
            </ul>
        </div>
    </nav>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "@Nurse/Default/nav.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  93 => 22,  90 => 21,  85 => 18,  82 => 17,  78 => 15,  75 => 14,  73 => 13,  64 => 6,  58 => 5,  52 => 4,  47 => 3,  44 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block nav %}
    <ul id=\"dropdown1\" class=\"dropdown-content\">
        {% if app.user %}{% else %}<li><a href=\"/login\">Se connecter</a></li>{% endif %}
        {% if app.user %}{% else %}<li><a href=\"/register\">S'enregister</a></li>{% endif %}
        {% if app.user %}<li><a href=\"/logout\">Se déconnecter</a></li>{% else %}{% endif %}
    </ul>

    <nav>
        <div class=\"nav-wrapper cyan darken-3\">
            <a href=\"/\" class=\"brand-logo\">Journal</a>
            <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">
                <li><a href=\"/\">Accueil</a></li>
                {% if app.user %}
                    {% if app.user.hasRole('ROLE_AUTEUR') %}
                        <li><a href=\"/user/ajouter\">Ajouter</a></li>
                    {% endif %}
                    {% if app.user.hasRole('ROLE_ADMIN') %}
                        <li><a href=\"/auteur/ajouter\">Ajouter</a></li>
                        <li><a href=\"/admin/admin\">Admin</a></li>
                    {% endif %}
                {% endif %}
                <li><a class=\"dropdown-trigger\" href=\"#!\" data-target=\"dropdown1\">{% if app.user %}{{ app.user.username }}{% else %}Connexion{% endif %}<i class=\"material-icons white-text right\">arrow_drop_down</i></a></li>
            </ul>
        </div>
    </nav>
{% endblock %}
", "@Nurse/Default/nav.html.twig", "/var/www/html/NurseOnline/src/NurseBundle/Resources/views/Default/nav.html.twig");
    }
}
